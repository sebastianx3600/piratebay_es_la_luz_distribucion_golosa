package logicaGolosa;

import java.util.ArrayList;

import logica.CalcularDistancia;
import logica.Entidad;
import logica.FuncionesParaArrayList;
import logica.ListaDeEntidad;

public class MayorCantidadDeCercanos implements LogicaGolosa{

	@Override
	public ArrayList<Integer> aplicar(ListaDeEntidad clientes, ListaDeEntidad centros, int cantCentrosParaElegir) {
		if(centros.getCantidadDeAgregados() == 0) {
			throw new IndexOutOfBoundsException("No hay Centros agregados");
		}
		if(clientes.getCantidadDeAgregados() == 0) {
			throw new IndexOutOfBoundsException("No hay ning�n Cliente agregado");
		}
		ArrayList<Integer> ret = FuncionesParaArrayList.inicializarArreglo(centros.getCantidadDeAgregados());
		Integer centroMasCercano;
		for(int nroCliente = 0; nroCliente < clientes.getCantidadDeAgregados(); nroCliente++ ) {
			centroMasCercano = indiceDelCentroMasCercano(clientes.getEntidad(nroCliente),centros);
			ret.set(centroMasCercano, ret.get(centroMasCercano)+1);
		}
		ret = FuncionesParaArrayList.getLasPosicionesMayores(ret,cantCentrosParaElegir);
		return ret;
	}

	private int indiceDelCentroMasCercano(Entidad cliente, ListaDeEntidad centros) {
		Integer indiceCentroMasCercano = 0;
		double distanciaMinimo;
		double nuevaDistancia;
		indiceCentroMasCercano = 0;
		distanciaMinimo = CalcularDistancia.getDistancia(cliente, centros.getEntidad(0));
		for(int nroCentro = 0; nroCentro < centros.getCantidadDeAgregados(); nroCentro++ ) {
			nuevaDistancia = CalcularDistancia.getDistancia(cliente, centros.getEntidad(nroCentro));
			if(nuevaDistancia < distanciaMinimo) {
				indiceCentroMasCercano = nroCentro;
				distanciaMinimo = nuevaDistancia;
			}
		}
		return indiceCentroMasCercano;
	}

}
