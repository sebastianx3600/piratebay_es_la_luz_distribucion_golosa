package logicaGolosa;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import logica.Entidad;
import logica.ListaDeEntidad;

public class MayorCantidadDeCercanosTest {

	LogicaGolosa logica;
	ListaDeEntidad clientes;
	ListaDeEntidad centros;
	
	@Before
	public void test() {
		logica = new MayorCantidadDeCercanos();
		clientes = new ListaDeEntidad();
		centros = new ListaDeEntidad();
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void aplicarSinNingunClienteTest() {
		centros.addEntidad(new Entidad("Centro1",0,0));
		logica.aplicar(clientes, centros, 1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void aplicarSinNingunCentroTest() {
		clientes.addEntidad(new Entidad("Pepe",0,0));
		logica.aplicar(clientes, centros, 1);
	}
	
	@Test
	public void HappyPathAplicarTest() {
		clientes.addEntidad(new Entidad("Leo",0,0));
		clientes.addEntidad(new Entidad("Lio",1,1));
		centros.addEntidad(new Entidad("Centro1",5,5));
		ArrayList<Integer> cercanos = logica.aplicar(clientes, centros, 1);
		assertEquals((int) cercanos.get(0),0);
	}
	
	@Test
	public void HappyPathAplicarSobreTresCentrosEscogiendoUnoTest() {
		clientes.addEntidad(new Entidad("Leo",0,0));
		clientes.addEntidad(new Entidad("Lio",1,1));
		centros.addEntidad(new Entidad("Centro1",0,1));
		centros.addEntidad(new Entidad("Centro2",5,5));
		clientes.addEntidad(new Entidad("Agus",6,6));
		clientes.addEntidad(new Entidad("Alejo",5,6));
		clientes.addEntidad(new Entidad("Franco",4,4));
		centros.addEntidad(new Entidad("Centro3",10,10));
		ArrayList<Integer> cercanos = logica.aplicar(clientes, centros, 1);
		assertEquals((int) cercanos.get(0),1);
	}
	
	@Test
	public void HappyPathAplicarSobreTresCentrosEscogiendoDosTest() {
		clientes.addEntidad(new Entidad("Leo",0,0));
		clientes.addEntidad(new Entidad("Lio",1,1));
		centros.addEntidad(new Entidad("Centro1",0,1));
		centros.addEntidad(new Entidad("Centro2",5,5));
		clientes.addEntidad(new Entidad("Agus",6,6));
		clientes.addEntidad(new Entidad("Alejo",5,6));
		clientes.addEntidad(new Entidad("Franco",4,4));
		centros.addEntidad(new Entidad("Centro3",10,10));
		ArrayList<Integer> cercanos = logica.aplicar(clientes, centros, 2);
		assertEquals((int) cercanos.get(1), 0);
	}
	
	@Test
	public void aplicarSinCentrosPorAbrirTest() {
		clientes.addEntidad(new Entidad("Leo",0,0));
		clientes.addEntidad(new Entidad("Lio",1,1));
		centros.addEntidad(new Entidad("Centro1",5,5));
		ArrayList<Integer> cercanos = logica.aplicar(clientes, centros, 0);
		assertEquals(cercanos.size(),0);
	}
	
}
