package logicaGolosa;

import java.util.ArrayList;

import logica.ListaDeEntidad;

public interface LogicaGolosa {

	public ArrayList<Integer> aplicar(ListaDeEntidad clientes, ListaDeEntidad centros, int cantCentrosParaElegir);
	
}
