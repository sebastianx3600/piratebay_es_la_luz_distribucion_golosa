package logica;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ListaDeEntidadTest {

	ListaDeEntidad lista;
	
	@Before
	public void inicializarLista() {
		lista = new ListaDeEntidad();
	}
	
	@Test
	public void agregarALaListaCantidadDeAgregadosTest() {
		agregarTresDistintosALista();
		assertEquals(3,lista.getCantidadDeAgregados());
	}
	
	@Test
	public void agregarALaListaCompararElPrimerAgregadoTest() {
		agregarTresDistintosALista();
		assertEquals(lista.getEntidad(0),new Entidad("Paco", 3, 3));
	}
	
	@Test
	public void agregarALaListaCompararElSegundoAgregadoTest() {
		agregarTresDistintosALista();
		assertEquals(lista.getEntidad(1),new Entidad("Marcos", 2, 2));
	}
	
	@Test
	public void agregarALaListaCompararElTercerAgregadoTest() {
		agregarTresDistintosALista();
		assertEquals(lista.getEntidad(2),new Entidad("Maria", 5, 6));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarALaListaUnRepetidoTest() {
		agregarTresDistintosALista();
		lista.addEntidad("Maria", 0, 0);
	}
	
	@Test
	public void estaAgregadoTest() {
		agregarTresDistintosALista();
		assertTrue(lista.yaEstaAgregado("Paco") && lista.yaEstaAgregado("Marcos") 
				&& lista.yaEstaAgregado("Maria"));
	}
	
	@Test
	public void noEstaAgregadoTest() {
		agregarTresDistintosALista();
		assertFalse(lista.yaEstaAgregado("Cesar"));
	}

	private void agregarTresDistintosALista() {
		lista.addEntidad("Paco", 3, 3);
		lista.addEntidad("Marcos", 2, 2);
		lista.addEntidad("Maria", 5, 6);
	}

}
