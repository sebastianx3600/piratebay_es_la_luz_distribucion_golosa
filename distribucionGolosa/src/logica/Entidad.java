package logica;

public class Entidad {
	
	double latitud, longitud;
	String nombre;

	public Entidad (String nombre, double latitud, double longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
		verificarNombre(nombre);
		this.nombre = nombre;
	}
	
	public double getLatitud() {
		return latitud;
	}

	public double getLongitud() {
		return longitud;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String toString() {
		return "<" + getNombre() + ">";
	}
	
	@Override
	public boolean equals(Object otroCliente) {
		if(!otroCliente.getClass().equals(Entidad.class)) {
			throw new IllegalArgumentException("No son del mismo tipo, Entidad");
		}
		return getNombre().equals(((Entidad) otroCliente).getNombre())
				&& getLatitud() == ((Entidad) otroCliente).getLatitud() 
				&& getLongitud() == ((Entidad) otroCliente).getLongitud();
	}
	
	public void verificarNombre(String nombre) {
		if(nombre == null) {
			throw new IllegalArgumentException("El nombre no puede ser null");
		}
		if(nombre.equals("")) {
			throw new IllegalArgumentException("El nombre no puede ser '', el nombre estaba vacio");
		}
	}

}
