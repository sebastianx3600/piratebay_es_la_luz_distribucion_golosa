package logica;

import java.util.ArrayList;

public class ListaDeEntidad {

	ArrayList<Entidad> lista;
	
	public ListaDeEntidad() {	//Lista de entidades, no permitimos que hayan dos con el mismo nombre
		lista = new ArrayList<Entidad>();
	}
	
	public void addEntidad(String nombre, double latitud, double longitud) {
		if(yaEstaAgregado(nombre)) {
			throw new IllegalArgumentException("Ya estaba agregado en la lista " + nombre);
		}
		lista.add(new Entidad(nombre, latitud, longitud));
	}
	
	public void addEntidad(Entidad entidad) {
		if(yaEstaAgregado(entidad.getNombre())) {
			throw new IllegalArgumentException("Ya estaba agregado en la lista " + entidad.getNombre());
		}
		lista.add(entidad);
	}
	
	public boolean yaEstaAgregado(String nombre) {
		boolean ret = false;
		for(Entidad e: lista) {
			ret = ret || e.getNombre().equals(nombre);
		}
		return ret;
	}
	
	public int getCantidadDeAgregados() {
		return lista.size();
	}
	
	public Entidad getEntidad(int nro) {
		if(esNumeroValido(nro)) {
			return lista.get(nro);
		}
		throw new IllegalArgumentException("El numero("+ nro + ") no es valido para la "
				+ "lista de tama�o"+getCantidadDeAgregados());
	}
	
	public boolean esNumeroValido(int nro) {
		return (nro < getCantidadDeAgregados() && nro>=0);
	}
	
	public ArrayList<Entidad> getListaDeEntidad(){
		return lista;
	}
	
	public String toString() {
		String ret = "";
		for(Entidad e: lista) {
			ret = ret + " " + e.toString();
		}
		return ret+"\n";
	}
}
