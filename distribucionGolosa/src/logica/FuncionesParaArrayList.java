package logica;

import java.util.ArrayList;

public class FuncionesParaArrayList {

	public static ArrayList<Integer> inicializarArreglo(int cantidadDeCeros) {
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for(int x = 0; x<cantidadDeCeros; x++) {
			ret.add(0);
		}
		return ret;
	}

	public static ArrayList<Integer> getLasPosicionesMayores(ArrayList<Integer> lista,int cantPos){
		ArrayList<Integer> ret = new ArrayList<Integer>();
		Integer indMayor;
		for(int x = 0; x < cantPos; x++){
			indMayor = getElIndiceDelMayor(lista);
			if(indMayor != null) {
				ret.add(indMayor);
				lista.set(indMayor, -1);				//Todos son mayor o igual a cero
			}
		}
		return ret;
	}
	
	private static Integer getElIndiceDelMayor(ArrayList<Integer> lista) {
		Integer mayor = 0;
		for(int y = 0; y<lista.size(); y++) {
			if(lista.get(mayor) < lista.get(y)) {
				mayor = y;
			}
		}
		return mayor;
	}
	
	public static Double getSumaDeValores(ArrayList<Double> lista) {
		Double acum = 0.0;
		for(int y = 0; y<lista.size(); y++) {
			acum = acum + lista.get(y);
		}
		return acum;
	}
	
	public static Double getMenorValor(ArrayList<Double> lista) {
		Double mayor = lista.get(0);
		for(int y = 0; y<lista.size(); y++) {
			if(mayor > lista.get(y)) {
				mayor = lista.get(y);
			}
		}
		return mayor;
	}
	
	public static Double getMayorValor(ArrayList<Double> lista) {
		Double mayor = lista.get(0);;
		for(int y = 0; y<lista.size(); y++) {
			if(mayor < lista.get(y)) {
				mayor = lista.get(y);
			}
		}
		return mayor;
	}
	
	public static Double getPromedio(ArrayList<Double> lista) {
		Double acum = 0.0;
		for(int y = 0; y<lista.size(); y++) {
			acum = acum + lista.get(y);
		}
		return acum/lista.size();
	}
}
