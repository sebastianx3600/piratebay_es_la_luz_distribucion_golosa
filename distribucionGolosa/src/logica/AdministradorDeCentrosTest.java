package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class AdministradorDeCentrosTest {

	AdministradorDeCentros admin;
	
	@Before
	public void crearDatosDefaultTest() {
		admin = new AdministradorDeCentros();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarUnClienteRepetidoTest() {
		admin.agregarNuevoCliente("Cliente0", 1, 1);
		admin.agregarNuevoCliente("Cliente0", 2, 2);
	}
	
	@Test
	public void agregarMultiplesClientesTest() {
		admin.agregarNuevoCliente("Cliente0", 1, 1);
		admin.agregarNuevoCliente("Cliente1", 1, 1);
		admin.agregarNuevoCliente("Cliente2", 1, 1);
		String [] nombres = {"Cliente0", "Cliente1", "Cliente2"};
		for(int x = 0; x<admin.getCantidadDeClientes(); x++) {
			assertTrue(admin.existeClienteConEseNombre(nombres[x]));
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarUnCentroRepetidoTest() {
		admin.agregarNuevoCentro("Centro0", 1, 1);
		admin.agregarNuevoCentro("Centro0", 2, 2);
	}
	
	@Test
	public void agregarMultiplesCentrosTest() {
		admin.agregarNuevoCentro("Centro0", 1, 1);
		admin.agregarNuevoCentro("Centro1", 1, 1);
		admin.agregarNuevoCentro("Centro2", 1, 1);
		String [] nombres = {"Centro0", "Centro1", "Centro2"};
		for(int x = 0; x<admin.getCantidadDeCentros(); x++) {
			assertTrue(admin.existeCentroConEseNombre(nombres[x]));
		}
	}
	
	/*
	 * Test de la logica golosa de clientes mas cercanos
	 */
	
	@Test
	public void seleccionDeUnCentroTest() {
		addDatosDefault();
		assertEquals(1, admin.seleccionarCentrosConMasClientesCercanos(1).size());
	}
	
	@Test
	public void seleccionDeUnCentroConLogicaGolosaGetNombreDekCentroTest() {
		addDatosDefault();
		ArrayList<Integer> elegidos = admin.seleccionarCentrosConMasClientesCercanos(1);
		assertEquals("Centro1",admin.getNombreCentros(elegidos.get(0)));
	}
	
	@Test
	public void seleccionDeDosCentrosTest() {
		addDatosDefault();
		assertEquals(2, admin.seleccionarCentrosConMasClientesCercanos(2).size());
	}
	
	@Test
	public void seleccionDeDosCentrosDatosTest() {
		addDatosDefault();
		ArrayList<Integer> elegidos = admin.seleccionarCentrosConMasClientesCercanos(2);
		assertEquals("Centro2",admin.getNombreCentros(elegidos.get(1)));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void seleccionarCentrosCantidadInvalidaTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(-1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void seleccionarCentrosCantidadInvalidaCeroTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(0);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void seleccionarCentrosCantidadInvalidaMayorTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(admin.getCantidadDeCentros()+1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void seleccionarCentrosSinClientesTest() {
		admin.agregarNuevoCentro("Centro0", 1, 1);
		admin.seleccionarCentrosConMasClientesCercanos(1);
	}
	
	/*
	 * 	getCentroMasCercanoElegido TEST
	 */
	
	@Test
	public void seleccionDeUnCentroYClientesMasCercanosAlCentroTest() {
		addDatosDefault();
		ArrayList<Integer> elegidos = admin.seleccionarCentrosConMasClientesCercanos(1);
		for(int x = 0; x<admin.getCantidadDeClientes(); x++) {
			assertEquals(elegidos.get(0), admin.getCentroMasCercanoElegido(x));
		}
	}
	
	@Test
	public void seleccionDeDosCentrosYClientesMasCercanosAlPrimerCentroTest() {
		addDatosDefault();
		ArrayList<Integer> elegidos = admin.seleccionarCentrosConMasClientesCercanos(2);
		for(int x = 0; x<3; x++) {
			assertEquals(elegidos.get(0), admin.getCentroMasCercanoElegido(x));
		}
	}
	
	@Test
	public void seleccionDeDosCentrosYClientesMasCercanosAlSegundoCentroTest() {
		addDatosDefault();
		ArrayList<Integer> elegidos = admin.seleccionarCentrosConMasClientesCercanos(2);
		for(int x = 3; x<admin.getCantidadDeClientes(); x++) {
			assertEquals(elegidos.get(1), admin.getCentroMasCercanoElegido(x));
		}
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void seleccionDeDosCentrosYClientesMasCercanosMalIndiceTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(2);
		admin.getCentroMasCercanoElegido(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void seleccionDeCentroMasCercanosTest() {
		addDatosDefault();
		admin.getCentroMasCercanoElegido(1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void seleccionDeCentroMasCercanosPeroSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente1", 1, 1);
		admin.getCentroMasCercanoElegido(0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void seleccionDeCentroMasCercanosPeroSinClientesTest() {
		admin.agregarNuevoCentro("Centro1", 2, 2);
		admin.getCentroMasCercanoElegido(0);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void seleccionDeCentrosYClientesMasCercanosMalIndiceTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(2);
		admin.getCentroMasCercanoElegido(-1);
	}
	
	@Test
	public void costoTotalTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(1).size();
		double costoTotal = 0;
		Entidad auxCentro = new Entidad("aux",2,2);
		Entidad auxCliente = new Entidad("aux",1,1);
		costoTotal = costoTotal + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",1,2);
		costoTotal = costoTotal + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",1,3);
		costoTotal = costoTotal + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",6,6);
		costoTotal = costoTotal + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",5,4);
		costoTotal = costoTotal + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",11,11);
		costoTotal = costoTotal + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		assertEquals(costoTotal, admin.getCostoDeLaSelecccion(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoTotalSinClientesTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.getCostoDeLaSelecccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoTotalSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCostoDeLaSelecccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoTotalSinSeleccionPrevioTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCostoDeLaSelecccion();
	}
	
	@Test
	public void costoMaximoTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(1).size();
		double costoMaximo = 0;
		Entidad auxCentro = new Entidad("aux",2,2);
		Entidad auxCliente = new Entidad("aux",11,11);
		costoMaximo = CalcularDistancia.getDistancia(auxCliente, auxCentro);
		assertEquals(costoMaximo,admin.getMayorCostoDeLaSeleccion(),0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoMaximoTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.getMayorCostoDeLaSeleccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoMaximoSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getMayorCostoDeLaSeleccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoMaximoSinSeleccionPrevioTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getMayorCostoDeLaSeleccion();
	}
	
	@Test
	public void costoMinimoTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(1).size();
		double costoMaximo = 0;
		Entidad auxCentro = new Entidad("aux",2,2);
		Entidad auxCliente = new Entidad("aux",1,2);
		costoMaximo = CalcularDistancia.getDistancia(auxCliente, auxCentro);
		assertEquals(costoMaximo,admin.getMenorCostoDeLaSeleccion(),0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoMinimoSinClientesTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.getMenorCostoDeLaSeleccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getMenorCostoDeLaSeleccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCostoMinimoSinSeleccionPrevioTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getMenorCostoDeLaSeleccion();
	}
	
	@Test
	public void costoPromedioSeleccionandoUnCentroTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(1).size();
		double costoProm = 0;
		Entidad auxCentro = new Entidad("aux",2,2);
		Entidad auxCliente = new Entidad("aux",1,1);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",1,2);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",1,3);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",6,6);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",5,4);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",11,11);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		costoProm = costoProm/admin.getCantidadDeClientes();
		assertTrue(costoProm == admin.getCostoPromedioDeLaSeleccion());
		assertEquals(costoProm,admin.getCostoPromedioDeLaSeleccion(),0);
	}
	
	@Test
	public void costoPromedioSeleccionandoDosCentrosTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(2).size();
		double costoProm = 0;
		Entidad auxCentro = new Entidad("aux",2,2);
		Entidad auxCliente = new Entidad("aux",1,1);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",1,2);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",1,3);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCentro = new Entidad("aux",5,5);
		auxCliente = new Entidad("aux",6,6);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",5,4);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		auxCliente = new Entidad("aux",11,11);
		costoProm = costoProm + CalcularDistancia.getDistancia(auxCliente, auxCentro);
		costoProm = costoProm/admin.getCantidadDeClientes();
		assertEquals(costoProm,admin.getCostoPromedioDeLaSeleccion(),0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarcostoPromedioCercaDelCentroSinClientesTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.getCostoPromedioDeLaSeleccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarcostoPromedioCercaDelCentroSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCostoPromedioDeLaSeleccion();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarcostoPromedioCercaDelCentroSinSeleccionPrevioTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCostoPromedioDeLaSeleccion();
	}
	
	@Test
	public void cantClientesPromedioCercaDelCentroTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(2);
		assertEquals(3,admin.getCantidadPromedioDeClientesCercaDeUnCentro(),0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesPromedioCercaDelCentroSinClientesTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.getCantidadPromedioDeClientesCercaDeUnCentro();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesPromedioCercaDelCentroSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCantidadPromedioDeClientesCercaDeUnCentro();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesPromedioCercaDelCentroSinSeleccionPrevioTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCantidadPromedioDeClientesCercaDeUnCentro();
	}
	
	@Test
	public void cantClientesMaximaCercaDelCentroTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(3);
		assertEquals(3, admin.getCantidadMaximaDeClientesCercaDeUnCentro());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesMaximaCercaDelCentroSinClientesTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.getCantidadMaximaDeClientesCercaDeUnCentro();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesMaximaCercaDelCentroSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCantidadMaximaDeClientesCercaDeUnCentro();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesMaximaCercaDelCentroSinSeleccionPrevioTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCantidadMaximaDeClientesCercaDeUnCentro();
	}
	
	@Test
	public void cantClientesMinimaCercaDelCentroTest() {
		addDatosDefault();
		admin.seleccionarCentrosConMasClientesCercanos(3);
		assertEquals(1, admin.getCantidadMinimaDeClientesCercaDeUnCentro());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesMinimaCercaDelCentroSinClientesTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.getCantidadMinimaDeClientesCercaDeUnCentro();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesMinimaCercaDelCentroSinCentrosTest() {
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCantidadMinimaDeClientesCercaDeUnCentro();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buscarCantidadDeClientesMinimaCercaDelCentroSinSeleccionPrevioTest() {
		admin.agregarNuevoCentro("Centro Solo", 7, 5);
		admin.agregarNuevoCliente("Cliente solo", 15, 12);
		admin.getCantidadMinimaDeClientesCercaDeUnCentro();
	}
	
	private void addDatosDefault() {
		admin.agregarNuevoCentro("Centro1", 2, 2);
		admin.agregarNuevoCliente("Cliente1", 1, 1);
		admin.agregarNuevoCliente("Cliente2", 1, 2);
		admin.agregarNuevoCliente("Cliente3", 1, 3);
		admin.agregarNuevoCentro("Centro2", 5, 5);
		admin.agregarNuevoCliente("Cliente4", 6, 6);
		admin.agregarNuevoCliente("Cliente5", 5, 4);
		admin.agregarNuevoCentro("CentroLejano", 10, 10);
		admin.agregarNuevoCliente("Cliente6", 11, 11);
	}

}
