package logica;

import static org.junit.Assert.*;

import org.junit.Test;

public class EntidadTest {
	
	Entidad entidad;
	
	@Test
	public void compararDosEntidadesIguales() {
		inicializarPCon2_2();
		Entidad entidad2 = new Entidad("Jose",2,2);
		assertEquals(entidad,entidad2);
	}
	
	@Test
	public void compararDosEntidadesConDistintasLatitudes() {
		inicializarPCon2_2();
		Entidad entidad2 = new Entidad("Jose",1,2);
		assertFalse(entidad.equals(entidad2));
	}
	
	@Test
	public void compararDosEntidadesDistintasLongitudes() {
		inicializarPCon2_2();
		Entidad entidad2 = new Entidad("Jose",2,1);
		assertFalse(entidad.equals(entidad2));
	}
	
	@Test
	public void compararDosEntidadesDistintoCompletamente() {
		inicializarPCon2_2();
		Entidad entidad2 = new Entidad("Jose",1,1);
		assertFalse(entidad.equals(entidad2));
	}
	
	@Test
	public void compararDosEntidadesConDistintosNombres() {
		inicializarPCon2_2();
		Entidad entidad2 = new Entidad("Norma",2,2);
		assertFalse(entidad.equals(entidad2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearEntidadSinNombre() {
		entidad = new Entidad("",2,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearEntidadSinNombreNull() {
		entidad = new Entidad(null,2,2);
	}

	private void inicializarPCon2_2() {
		entidad = new Entidad("Jose",2,2);
	}

}
