package logica;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalcularDistanciaTest {

	@Test
	public void compararDistanciaTest() {
		Entidad c = new Entidad("A",2,2);
		Entidad c2 = new Entidad("A",4,4);
		assertEquals(Math.sqrt(8), CalcularDistancia.getDistancia(c,c2), 0);
	}
	
	@Test
	public void compararDistanciaIgualACeroTest() {
		Entidad c = new Entidad("A",4,4);
		Entidad c2 = new Entidad("A",4,4);
		assertEquals(0, CalcularDistancia.getDistancia(c,c2), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void compararUnPuntoConUnNullTest() {
		Entidad c = new Entidad("A",4,4);
		Entidad c2 = null;
		CalcularDistancia.getDistancia(c,c2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void compararUnNullConUnPuntoTest() {
		Entidad c = null;
		Entidad c2 = new Entidad("A",4,4);
		CalcularDistancia.getDistancia(c,c2);
	}

}
