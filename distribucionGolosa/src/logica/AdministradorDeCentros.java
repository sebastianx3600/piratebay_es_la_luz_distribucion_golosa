package logica;

import java.util.ArrayList;

import datos.JsonListaDeEntidad;
import logicaGolosa.LogicaGolosa;
import logicaGolosa.MayorCantidadDeCercanos;

public class AdministradorDeCentros {
	
	ListaDeEntidad clientes;
	ListaDeEntidad centros;
	
	String nombreArchivoClientes = "listaDeClientes";
	String nombreArchivoCentros = "listaDeCentros";
	String nombreArchivoResultados = "listaDeCentrosResultado";
	
	ArrayList<Integer> centrosEscogidos;
	ArrayList<Integer> cantClientesCercaACentrosEscogidos;
	
	public AdministradorDeCentros() {
		centrosEscogidos = new ArrayList<Integer>();
		cantClientesCercaACentrosEscogidos = new ArrayList<Integer>();
		clientes = new ListaDeEntidad();
		centros = new ListaDeEntidad();
	}
	
	public void agregarNuevoCliente(String nombre, double latitud, double longitud) {
		clientes.addEntidad(nombre, latitud, longitud);
	}
	
	public void agregarNuevoCentro(String nombre, double latitud, double longitud) {
		centros.addEntidad(nombre, latitud, longitud);
	}
	
	public ArrayList<Integer> seleccionarCentrosConMasClientesCercanos(int cantCentros) {		//Aqui se emplea la logica golosa
		if(cantCentros<=0 || cantCentros>this.getCantidadDeCentros()) {
			throw new IndexOutOfBoundsException("El numero para el arreglo (" + cantCentros + ") no es valido"
					+ ", cantidad de centros= " + this.getCantidadDeCentros());
		}
		verificarQueHayaClientes();
		centrosEscogidos = seleccionarCentrosConMasClientesCercanos(new MayorCantidadDeCercanos(),cantCentros);
		cantClientesCercaACentrosEscogidos = cantidadDeClientesCerca();
		guardarResultadoEnUnArchivo();
		return centrosEscogidos;
	}
	
	public Integer getCentroMasCercanoElegido(int nroCliente) {
		verificarQueHayaClientes();
		if(this.getCantidadDeCentros()==0) {
			throw new IllegalArgumentException("No hay centros cargados");
		}
		if(this.getCantidadDeClientes()<=nroCliente || nroCliente < 0) {
			throw new IndexOutOfBoundsException("Numero de cliente ("+nroCliente+") no valido, "
					+ "cantidad de clientes :"+getCantidadDeClientes());
		}
		verificarQueHayaCentrosEscogidos();
		Integer indiceMinimo = centrosEscogidos.get(0);
		double distanciaMinima = calcularDistancia(nroCliente, indiceMinimo);
		double distanciaNueva;
		for(int indCentro = 0; indCentro < centrosEscogidos.size(); indCentro++) {
			distanciaNueva = calcularDistancia(nroCliente, centrosEscogidos.get(indCentro));
			if(distanciaNueva < distanciaMinima){
				indiceMinimo = centrosEscogidos.get(indCentro);
				distanciaMinima = distanciaNueva;
			}
		}
		return indiceMinimo;
	}
	
	public String getResultadoString() {
		return recuperarCentros().toString();
	}
	
	/*
	 * Estadisticas
	 */
	
	public double getCostoDeLaSelecccion() {
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		return FuncionesParaArrayList.getSumaDeValores(this.getListaDeMejoresDistancia());
	}
	
	public double getMayorCostoDeLaSeleccion() {
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		return FuncionesParaArrayList.getMayorValor(this.getListaDeMejoresDistancia());
	}
	
	public double getMenorCostoDeLaSeleccion() {
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		return FuncionesParaArrayList.getMenorValor(this.getListaDeMejoresDistancia());
	}
	
	public double getCostoPromedioDeLaSeleccion() {
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		return FuncionesParaArrayList.getPromedio(this.getListaDeMejoresDistancia());
	}
	
	public int getCantidadMaximaDeClientesCercaDeUnCentro() {
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		int ret = cantClientesCercaACentrosEscogidos.get(0);
		for(int cantClientes: cantClientesCercaACentrosEscogidos) {
			if(ret < cantClientes) {
				ret = cantClientes;
			}
		}
		return ret;
	}
	
	public int getCantidadMinimaDeClientesCercaDeUnCentro() {
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		int ret = cantClientesCercaACentrosEscogidos.get(0);
		for(int cantClientes: cantClientesCercaACentrosEscogidos) {
			if(ret > cantClientes) {
				ret = cantClientes;
			}
		}
		return ret;
	}
	
	public double getCantidadPromedioDeClientesCercaDeUnCentro() {
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		double ret = 0;
		for(int cantClientes: cantClientesCercaACentrosEscogidos) {
			ret = ret + cantClientes;
		}
		ret = ret/this.centrosEscogidos.size();
		return ret;
	}
	
	public void cargarListasDeClientes() {
		clientes = JsonListaDeEntidad.getLista(nombreArchivoClientes);
	}
	
	public void cargarListasDeCentros() {
		centros = JsonListaDeEntidad.getLista(nombreArchivoCentros);
	}
	
	public void borrarListaCliente() {
		clientes = new ListaDeEntidad();
	}
	
	public void borrarListaCentro() {
		centros = new ListaDeEntidad();
	}
	
	public void guardarListaDeCliente() {
		JsonListaDeEntidad.guardarLista(clientes,nombreArchivoClientes);
	}
	
	public void guardarListaDeCentro() {
		JsonListaDeEntidad.guardarLista(centros,nombreArchivoCentros);
	}
	
	public int getCantidadDeClientes() {
		return clientes.getCantidadDeAgregados();
	}
	
	public int getCantidadDeCentros() {
		return centros.getCantidadDeAgregados();
	}
	
	public String getNombreCliente(int nroCliente) {
		return clientes.getEntidad(nroCliente).getNombre();
	}
	
	public double getLatitudCliente(int nroCliente) {
		return clientes.getEntidad(nroCliente).getLatitud();
	}
	
	public double getLongitudCliente(int nroCliente) {
		return clientes.getEntidad(nroCliente).getLongitud();
	}
	
	public boolean existeClienteConEseNombre(String nombre) {
		return clientes.yaEstaAgregado(nombre);
	}
	
	public String getNombreCentros(int nroCentro) {
		return centros.getEntidad(nroCentro).getNombre();
	}
	
	public double getLatitudCentros(int nroCentro) {
		return centros.getEntidad(nroCentro).getLatitud();
	}
	
	public double getLongitudCentros(int nroCentro) {
		return centros.getEntidad(nroCentro).getLongitud();
	}
	
	public boolean existeCentroConEseNombre(String nombre) {
		return centros.yaEstaAgregado(nombre);
	}
	
	public String pantalla() {
		String ret = "";
		ret = clientes.toString()+"\n";
		ret = ret + centros.toString()+"\n";
		return ret;
	}

	private void verificarQueHayaCentrosEscogidos() {
		if(centrosEscogidos == null || centrosEscogidos.size() == 0) {
			throw new IllegalArgumentException("No se selecciono la cantidad de centros previamente");
		}
	}

	private void verificarQueHayaClientes() {
		if(this.getCantidadDeClientes()==0) {
			throw new IllegalArgumentException("No hay clientes cargados");
		}
	}
	
	private void verificarQueHayaCentros() {
		if(this.getCantidadDeCentros()==0) {
			throw new IllegalArgumentException("No hay centros cargados");
		}
	}
	
	private ArrayList<Integer> seleccionarCentrosConMasClientesCercanos(LogicaGolosa logicaGolosa, int cantCentrosElegir) {
		return logicaGolosa.aplicar(clientes, centros, cantCentrosElegir);
	}
	
	private double calcularDistancia(int nroCliente, int indCentro) {
		return CalcularDistancia.getDistancia(clientes.getEntidad(nroCliente), centros.getEntidad(indCentro));
	}
	
	private void guardarResultadoEnUnArchivo() {
		ListaDeEntidad centrosAGuardar = recuperarCentros();
		JsonListaDeEntidad.guardarLista(centrosAGuardar,nombreArchivoResultados);
	}
	
	private ListaDeEntidad recuperarCentros() {
		verificarQueHayaCentrosEscogidos();
		ListaDeEntidad ret = new ListaDeEntidad();
		for(Integer i: centrosEscogidos) {
			ret.addEntidad(centros.getEntidad(i));
		}
		return ret;
	}
	
	private ArrayList<Double> getListaDeMejoresDistancia(){
		ArrayList<Double> ret = new ArrayList<Double>();
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		for(int indCliente = 0; indCliente<this.getCantidadDeClientes(); indCliente++) {
			ret.add(calcularDistancia(indCliente,getCentroMasCercanoElegido(indCliente)));
		}
		return ret;
	}
	
	private ArrayList<Integer> cantidadDeClientesCerca(){
		verificarQueHayaClientes();
		verificarQueHayaCentros();
		verificarQueHayaCentrosEscogidos();
		ArrayList<Integer> ret = FuncionesParaArrayList.inicializarArreglo(centrosEscogidos.size());
		int nroCentroMasCercano;
		for(int indCliente = 0; indCliente<this.getCantidadDeClientes(); indCliente++) {
			nroCentroMasCercano = this.getCentroMasCercanoElegido(indCliente);
			ret.set(centrosEscogidos.indexOf(nroCentroMasCercano), ret.get(centrosEscogidos.indexOf(nroCentroMasCercano))+1);
		}
		return ret;
	}

}
