package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class FuncionesParaArrayListTest {
	
	@Test
	public void inicializarArregloVacioTest() {
		ArrayList<Integer> a = FuncionesParaArrayList.inicializarArreglo(0);
		assertEquals(a.size(),0);
	}
	
	@Test
	public void inicializarArregloTest() {
		ArrayList<Integer> a = FuncionesParaArrayList.inicializarArreglo(20);
		assertEquals(a.size(),20);
	}
	
	@Test 
	public void getLasPosicionesMayoresTest() {
		ArrayList<Integer> cantClinetesCentro = new ArrayList<Integer>();
		cantClinetesCentro.add(1);
		cantClinetesCentro.add(2);	//dos centros, el primero con un cliente el seg con 2
		ArrayList<Integer> posMayores = FuncionesParaArrayList.getLasPosicionesMayores(cantClinetesCentro, 2);
		assertEquals((long)posMayores.get(0),1); //el indice del mayor es el 1=2
	}

	@Test
	public void getLasPosicionesMayoresVacioTest() {
		ArrayList<Integer> cantClinetesCentro = new ArrayList<Integer>();
		ArrayList<Integer> posMayores = FuncionesParaArrayList.getLasPosicionesMayores(cantClinetesCentro, 0);
		assertEquals(posMayores.size(),0);
	}
	
	@Test
	public void getSumaDeValoresTest() {
		ArrayList<Double> lista = new ArrayList<Double>();
		lista.add(1.0);
		lista.add(2.0);
		double valor = FuncionesParaArrayList.getSumaDeValores(lista);
		//aserEquals(double actual,expecte,delta) el delta es grados de precision
		assertEquals(valor,3.0,0);
	}
	
	@Test(expected = NullPointerException.class)
	public void getSumaValoresNuloTest() {
		FuncionesParaArrayList.getSumaDeValores(null);
	}
	
	@Test
	public void getMenorValorTest() {
		ArrayList<Double> lista = new ArrayList<Double>();
		lista.add(1.0);
		lista.add(2.0);
		double valor = FuncionesParaArrayList.getMenorValor(lista);
		assertEquals(valor,1.0,0);
	}
	
	@Test(expected = NullPointerException.class)
	public void getMenorValorVacioTest() {
		FuncionesParaArrayList.getMenorValor(null);
	}
	
	@Test
	public void getMayorValorTest() {
		ArrayList<Double> lista = new ArrayList<Double>();
		lista.add(1.0);
		lista.add(2.0);
		double valor = FuncionesParaArrayList.getMayorValor(lista);
		assertEquals(valor,2.0,0);
	}
	
	@Test(expected = NullPointerException.class)
	public void getMayorValorVacioTest() {
		FuncionesParaArrayList.getMayorValor(null);
	}
	
	@Test
	public void getPromedioTest() {
		ArrayList<Double> lista = new ArrayList<Double>();
		lista.add(2.0);
		lista.add(4.0);
		assertEquals(3.0,FuncionesParaArrayList.getPromedio(lista),0);
	}
	
	@Test(expected = NullPointerException.class)
	public void getPromedioNuloTest() {
		FuncionesParaArrayList.getPromedio(null);
	}
}
