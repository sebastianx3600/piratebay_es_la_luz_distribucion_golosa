package logica;

public class CalcularDistancia {

	public static double getDistancia(Entidad punto, Entidad otroPunto) {
		if(punto == null || otroPunto == null) {
			throw new IllegalArgumentException("Uno de los puntos es null");
		}
		double ret;
		double aux;
		ret = punto.getLatitud() - otroPunto.getLatitud();
		ret = ret*ret;
		aux =  punto.getLongitud() - otroPunto.getLongitud();
		aux = aux * aux;
		ret = ret+aux;
		ret = Math.sqrt(ret);
		return ret;
	}

}
