package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import logica.ListaDeEntidad;

public class JsonListaDeEntidad {
	
	public static void guardarLista(ListaDeEntidad nuevaLista, String archivoDestino) {
		guardarEnJSON(nuevaLista ,getDireccion()+archivoDestino);
	}
	
	private static void guardarEnJSON(ListaDeEntidad nuevaLista, String archivoDestino)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(nuevaLista);
		try
		{
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(json);
			writer.close();
		}catch(Exception e) {}

	}
	
	public static ListaDeEntidad getLista(String archivo) {
		return leerJSON(getDireccion()+archivo);
	}
	
	private static ListaDeEntidad leerJSON(String archivo)
	{
		if(!existArchivo(archivo)) {
			return new ListaDeEntidad();
		}
		Gson gson = new Gson();
		ListaDeEntidad ret = null;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, ListaDeEntidad.class);
		}
		catch (Exception e) {}
		return ret;
	}
	
	private static boolean existArchivo(String nombreArchivo) {
		File archivo = new File(nombreArchivo);
		return archivo.exists();
	}
	
	private static String getDireccion() {
		return "src/datos/";
	}
}
