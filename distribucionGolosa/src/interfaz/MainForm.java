package interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import logica.AdministradorDeCentros;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import java.awt.Font;

public class MainForm {

	private JFrame frame;
	private AdministradorDeCentros admin;

	private JPanel panelMapa;
	private JPanel panelControles;

	private ArrayList<Coordinate> coordenadasClientes;
	private ArrayList<Coordinate> coordenadasCentros;
	private ArrayList<MapPolygonImpl> caminos = new ArrayList<MapPolygonImpl>();
	private MapPolygonImpl camino;
	private JMapViewer mapa;

	private ButtonGroup grupoDeBotones;
	private JButton cantidadCentrosButton;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	private JButton btnNewButton_4;
	private JButton btnNewButton_5;

	private JLabel lblNewLabel;
	private JLabel labelResultados = new JLabel("-");
	private JLabel labelMayorCosto = new JLabel("Mayor costo: 0");
	private JLabel labelMenorCosto = new JLabel("Menor costo: 0");
	private JLabel labelCostoPromedio = new JLabel("Costo prom: 0");

	private JLabel labelMaxCliente;
	private JLabel labelMinCliente;
	private JLabel labelPromClientes;

	JRadioButton rdbtnNewRadioButton;
	JRadioButton rdbtnNewRadioButton_1;
	private JLabel labelCosto;

	private JSpinner spinner;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() {
		initialize();
	}

	private void initialize() {
		admin = new AdministradorDeCentros();

		frame = new JFrame();
		frame.setBounds(100, 100, 801, 778);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		panelMapa = new JPanel();
		panelMapa.setBounds(10, 11, 437, 621);
		frame.getContentPane().add(panelMapa);

		mapa = new JMapViewer();
		mapa.setZoomContolsVisible(false);
		Coordinate coordinate = new Coordinate(5, 7);
		mapa.setBounds(10, 11, 437, 601);
		mapa.setDisplayPosition(coordinate, 5);
		frame.getContentPane().add(mapa);

		panelControles = new JPanel();
		panelControles.setBounds(457, 11, 437, 621);
		frame.getContentPane().add(panelControles);
		panelControles.setLayout(null);

		spinner = new JSpinner(new SpinnerNumberModel(0, 0, admin.getCantidadDeCentros(), 1));
		spinner.setBounds(116, 7, 43, 23);
		panelControles.add(spinner);

		setearLabels();

		setearRadioButton();

		setearBotones();

		detectarCoordenadas();
	}

	private void cargarPuntos() {
		coordenadasClientes = new ArrayList<Coordinate>();
		coordenadasCentros = new ArrayList<Coordinate>();
		cargarClientesEnElMapa();
		cargarCentrosEnElMapa();
		limpiarLabelResultados();
	}

	private void cargarCentrosEnElMapa() {
		double posX;
		double posY;
		String nombre;
		for (int x = 0; x < admin.getCantidadDeCentros(); x++) { // AGREGAMOS LOS CENTROS
			posX = admin.getLongitudCentros(x);
			posY = admin.getLatitudCentros(x);
			nombre = admin.getNombreCentros(x);
			Coordinate markeradd = (Coordinate) new Coordinate(posX, posY);
			coordenadasCentros.add(markeradd);
			MapMarker mapMarker = new MapMarkerDot(nombre, markeradd);
			mapMarker.getStyle().setColor(Color.BLUE);
			mapMarker.getStyle().setBackColor(Color.BLUE);
			mapa.addMapMarker(mapMarker);
		}
		actualizarSpinner();
	}

	private void cargarClientesEnElMapa() {
		double posX;
		double posY;
		String nombre;
		for (int x = 0; x < admin.getCantidadDeClientes(); x++) { // AGREGAMOS LOS CLIENTES
			posX = admin.getLongitudCliente(x);
			posY = admin.getLatitudCliente(x);
			nombre = admin.getNombreCliente(x);
			Coordinate markeradd = (Coordinate) new Coordinate(posX, posY);
			coordenadasClientes.add(markeradd);
			mapa.addMapMarker(new MapMarkerDot(nombre, markeradd));
		}
	}

	private void dibujarCamino(int nroCentro, int nroCliente) {
		camino = new MapPolygonImpl(coordenadasClientes.get(nroCliente), coordenadasCentros.get(nroCentro),
				coordenadasClientes.get(nroCliente));
		camino.getStyle().setColor(Color.RED);
		caminos.add(camino);
		mapa.addMapPolygon(camino);
	}

	private void eliminarCaminos() {
		limpiarLabelResultados();
		for (MapPolygonImpl c : caminos) {
			mapa.removeMapPolygon(c);
		}
		caminos = new ArrayList<MapPolygonImpl>();
	}

	private void eliminarPuntos() {
		mapa.removeAllMapMarkers();
	}

	private void detectarCoordenadas() {
		mapa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				limpiarLabelResultados();
				eliminarCaminos();
				if (e.getButton() == MouseEvent.BUTTON1) {
					Coordinate markeradd = (Coordinate) mapa.getPosition(e.getPoint());
					String tipo = grupoDeBotones.getSelection().getActionCommand();
					String nombre;
					if (tipo.equals("Cliente")) {
						nombre = JOptionPane.showInputDialog("Nombre del Cliente: ");
					} else {
						nombre = JOptionPane.showInputDialog("Nombre del Centro: ");
					}
					if (nombre == null || nombre.equals("")) {
						JOptionPane.showMessageDialog(null, "Escribe un nombre!");
						return;
					}
					if (tipo.equals("Cliente")) {
						if (admin.existeClienteConEseNombre(nombre)) {
							JOptionPane.showMessageDialog(null, "El cliente con el nombre " + nombre + " ya existe!");
							return;
						}
						if (coordenadasClientes == null) {
							coordenadasClientes = new ArrayList<Coordinate>();
						}
						coordenadasClientes.add(markeradd);
						admin.agregarNuevoCliente(nombre, markeradd.getLon(), markeradd.getLat());
						mapa.addMapMarker(new MapMarkerDot(nombre, markeradd));
						return;
					}
					if (tipo.equals("Centro")) {
						if (admin.existeCentroConEseNombre(nombre)) {
							JOptionPane.showMessageDialog(null, "El Centro con el nombre " + nombre + " ya existe!");
							return;
						}
						if (coordenadasCentros == null) {
							coordenadasCentros = new ArrayList<Coordinate>();
						}
						coordenadasCentros.add(markeradd);
						admin.agregarNuevoCentro(nombre, markeradd.getLon(), markeradd.getLat());
						MapMarker mapMarker = new MapMarkerDot(nombre, markeradd);
						mapMarker.getStyle().setColor(Color.BLUE);
						mapMarker.getStyle().setBackColor(Color.BLUE);
						mapa.addMapMarker(mapMarker);

						actualizarSpinner();
					}
				}
			}

		});
	}

	private void actualizarSpinner() {
		panelControles.remove(spinner);
		panelControles.updateUI();
		spinner = new JSpinner(new SpinnerNumberModel(0, 0, admin.getCantidadDeCentros(), 1));
		spinner.setBounds(116, 7, 43, 23);
		panelControles.add(spinner);
	}

	private void setearLabels() {
		lblNewLabel = new JLabel("Cantidad centros:");
		lblNewLabel.setBounds(10, 11, 96, 14);
		panelControles.add(lblNewLabel);
		labelResultados.setBounds(20, 623, 689, 35);
		frame.getContentPane().add(labelResultados);
		labelResultados.setHorizontalAlignment(SwingConstants.CENTER);

		labelCosto = new JLabel("Costo: 0");
		labelCosto.setBounds(10, 173, 202, 14);
		panelControles.add(labelCosto);

		labelMayorCosto = new JLabel("Mayor costo: 0");
		labelMayorCosto.setBounds(10, 197, 202, 14);
		panelControles.add(labelMayorCosto);

		labelMenorCosto = new JLabel("Menor costo: 0");
		labelMenorCosto.setBounds(10, 221, 202, 14);
		panelControles.add(labelMenorCosto);

		labelCostoPromedio = new JLabel("Costo prom: 0");
		labelCostoPromedio.setBounds(10, 245, 202, 14);
		panelControles.add(labelCostoPromedio);

		labelMaxCliente = new JLabel("Max. clientes: 0");
		labelMaxCliente.setBounds(10, 269, 202, 14);
		panelControles.add(labelMaxCliente);

		labelMinCliente = new JLabel("Min. clientes: 0");
		labelMinCliente.setBounds(10, 293, 202, 14);
		panelControles.add(labelMinCliente);

		labelPromClientes = new JLabel("Prom. Clientes: 0");
		labelPromClientes.setBounds(10, 317, 212, 14);
		panelControles.add(labelPromClientes);

		JLabel labelEstadisticas = new JLabel("Estadisticas");
		labelEstadisticas.setBounds(10, 139, 118, 14);
		panelControles.add(labelEstadisticas);

	}

	private void setearRadioButton() {
		rdbtnNewRadioButton = new JRadioButton("Cliente");
		rdbtnNewRadioButton.setBounds(10, 81, 84, 23);
		panelControles.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setActionCommand("Cliente");
		rdbtnNewRadioButton.setSelected(true);

		rdbtnNewRadioButton_1 = new JRadioButton("Centro de distribucion");
		rdbtnNewRadioButton_1.setBounds(96, 81, 131, 23);
		panelControles.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setActionCommand("Centro");

		grupoDeBotones = new ButtonGroup();
		grupoDeBotones.add(rdbtnNewRadioButton);
		grupoDeBotones.add(rdbtnNewRadioButton_1);
	}

	private void setearBotones() {
		cantidadCentrosButton = new JButton("Aplicar centros");
		cantidadCentrosButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (coordenadasClientes == null) {
					JOptionPane.showMessageDialog(null, "Se deben cargar o setear los clientes y los centros primero");
					return;
				}
				for (MapPolygonImpl c : caminos) // Primero eliminos los que hay
				{
					mapa.removeMapPolygon(c);
				}
				Integer cantCentro = (Integer) spinner.getValue();
				caminos = new ArrayList<MapPolygonImpl>();
				int nroCentro;
				if (admin.getCantidadDeClientes() == 0) {
					JOptionPane.showMessageDialog(null, "No hay clientes");
					return;
				}
				limpiarLabelResultados();
				if (cantCentro > admin.getCantidadDeCentros()) {
					JOptionPane.showMessageDialog(null,
							"La cantidad de centros que se pide es mayor que los centros cargados");
					return;
				}
				if (cantCentro <= 0) {
					JOptionPane.showMessageDialog(null, "La cantidad de centros que se pide no es valida");
					return;
				}
				admin.seleccionarCentrosConMasClientesCercanos(cantCentro);
				for (int i = 0; i < coordenadasClientes.size(); i++) {
					nroCentro = admin.getCentroMasCercanoElegido(i);
					dibujarCamino(nroCentro, i);
				}
				actualizarLabelCosto();
			}
		});

		cantidadCentrosButton.setBounds(10, 41, 118, 23);
		panelControles.add(cantidadCentrosButton);

		btnNewButton = new JButton("Cargar clientes");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.cargarListasDeClientes();
				eliminarCaminos();
				eliminarPuntos();
				cargarPuntos();
			}
		});
		btnNewButton.setBounds(138, 353, 118, 23);
		panelControles.add(btnNewButton);

		btnNewButton_1 = new JButton("Cargar centros");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.cargarListasDeCentros();
				eliminarCaminos();
				eliminarPuntos();
				cargarPuntos();
				actualizarSpinner();
			}
		});
		btnNewButton_1.setBounds(0, 353, 128, 23);
		panelControles.add(btnNewButton_1);

		btnNewButton_2 = new JButton("Guardar clientes");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.guardarListaDeCliente();
			}
		});
		btnNewButton_2.setBounds(138, 386, 118, 23);
		panelControles.add(btnNewButton_2);

		btnNewButton_3 = new JButton("Guardar centros");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.guardarListaDeCentro();
			}
		});
		btnNewButton_3.setBounds(0, 386, 128, 23);
		panelControles.add(btnNewButton_3);

		btnNewButton_4 = new JButton("Borrar clientes");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.borrarListaCliente();
				eliminarCaminos();
				eliminarPuntos();
				cargarPuntos();
			}
		});
		btnNewButton_4.setBounds(138, 419, 118, 23);
		panelControles.add(btnNewButton_4);

		btnNewButton_5 = new JButton("Borrar centros");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				admin.borrarListaCentro();
				eliminarCaminos();
				eliminarPuntos();
				cargarPuntos();
				actualizarSpinner();
			}
		});
		btnNewButton_5.setBounds(0, 419, 128, 23);
		panelControles.add(btnNewButton_5);

	}

	private void limpiarLabelResultados() {
		labelResultados.setText("");
		labelCosto.setText("Costo: 0");
		labelMayorCosto.setText("Mayor costo: 0");
		labelMenorCosto.setText("Menor costo: 0");
		labelCostoPromedio.setText("Costo prom: 0");
		labelMaxCliente.setText("Max. clientes: 0");
		labelMinCliente.setText("Min. clientes: 0");
		labelPromClientes.setText("Prom. Clientes: 0");
	}

	private void actualizarLabelCosto() {
		labelResultados.setText(admin.getResultadoString());
		labelCosto.setText("Costo: " + admin.getCostoDeLaSelecccion());
		labelMayorCosto.setText("Mayor costo: " + admin.getMayorCostoDeLaSeleccion());
		labelMenorCosto.setText("Menor costo: " + admin.getMenorCostoDeLaSeleccion());
		labelCostoPromedio.setText("Costo prom: " + admin.getCostoPromedioDeLaSeleccion());
		labelMaxCliente.setText("Max. clientes: " + admin.getCantidadMaximaDeClientesCercaDeUnCentro());
		labelMinCliente.setText("Min. clientes: " + admin.getCantidadMinimaDeClientesCercaDeUnCentro());
		labelPromClientes.setText("Prom.: " + admin.getCantidadPromedioDeClientesCercaDeUnCentro());
	}
}
